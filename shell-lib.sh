#!/bin/bash
# PPWT
# Copyright (C) Eskild Hustvedt 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ====== A NOTE ON INTERFACE STABILITY ======
# All of the functions in this file are included when you do
# '. "$PPWT_SHELL_LIB" || exit 1' in your scripts. However, all functions that
# are prefixed with _, for instance _URLHash, are considered private. You
# should not use them. Their interface is not stable and may change from
# version to version. If you still insist on using them, do so at your own
# peril. If you need functionality not exported by the public functions, open a
# feature request instead.
# ======                               ======

_PPWT_APIVERSION_CALLED=0

# Sets the API version of a script. This will exit loudly if the script
# requests a version that is not supported.
function APIVersion
{
    local VERSION="$1"
    _PPWT_APIVERSION_CALLED=1
    if [ "$VERSION" -gt "2" ]; then
        _errOut "Unsupported shell-lib API version requested: $VERSION"
    fi
    return 0
}

# Checks if any of the provided URLs has changed since last time using headers
function hasChangedURLHeaders
{
    local _return=1
    local ENTRY
    for ENTRY in "$@"; do
        if _hasChangedURLHeaders "$ENTRY"; then
            _return=0
        fi
    done
    return $_return
}

# Checks if a single URL has changed since last time using headers
function _hasChangedURLHeaders
{
    local URL="$1"
    local NAME="$(_sanitizeForPath "$URL")"
    local CONTENT="$(URLHeaderSlurp "$URL" |egrep -i '^(ETAG|Last-Modified|Content-Length|Content-Type)'|sha512sum)"
    _checkStateFileAgainstCurrent "$CONTENT" "URL Headers" "$URL"
    local R=$?
    return $R
}

# Checks if any of the provided URLs has changed since last time
function hasChangedURL
{
    local _return=1
    local ENTRY
    for ENTRY in "$@"; do
        if _hasChangedURL "$ENTRY"; then
            _return=0
        fi
    done
    return $_return
}

# Adds an action limit
function max_daily_actions
{
    local maxActions=$1
    local dailyActionsCountFile="$PPWT_STATE_DIR/maxDailyActionsCount"
    local dailyActionsDateFile="$PPWT_STATE_DIR/maxDailyActionsDate"
    local currentActions="$(slurp "$dailyActionsCountFile")"
    if [ "$currentActions" == "" ]; then
        currentActions=0
    fi
    local currentDate="$(slurp "$dailyActionsDateFile")"
    if [ "$currentDate" == "$(date '+%Y-%m-%d')" ]; then
        if [ "$currentActions" == "$maxActions" ] || [ "$currentActions" -gt "$maxActions" ]; then
            vecho "Exiting because of max_daily_actions limit ($currentActions vs $maxActions)"
            exit 0
        fi
    fi
}

# Checks if a single URL has changed since last time
function _hasChangedURL
{
    local URL="$1"
    local NAME="$(_sanitizeForPath "$URL")"
    local CONTENT="$(_URLHash "$URL")"
    _checkStateFileAgainstCurrent "$CONTENT" "URL" "$URL"
    return $?
}

# Checks if a iCalendar URL has changed since last time
function hasChangedCalendarURL
{
    for ENTRY in "$@"; do
        if hasChangedFilteredURL "$ENTRY" "DTSTAMP"; then
            return 0
        fi
    done
    return 1
}

# Checks if a URL, with a filter applied, has changed since last time
# The filter is applied using 'egrep -v'
function hasChangedFilteredURL
{
    local URL="$1"
    local FILTER="$2"
    local NAME="$(_sanitizeForPath "$URL")"
    local CONTENT="$(URLslurp "$URL" |egrep -v "$FILTER"|sha512sum)"
    _checkStateFileAgainstCurrent "$CONTENT" "Filter URL" "$URL"
    return $?
}

# Verifies some content against a state file
function _checkStateFileAgainstCurrent
{
    local CURRENTCONTENT="$1"
    local TYPE="$2"
    local FOR="$3"
    local STATE_PATH="$(StateFile "$FOR" "$TYPE")"
    if [ ! -e "$STATE_PATH" ]; then
        vecho "No pre-existing $TYPE-state for '$FOR', writing state and considering it unchanged"
        spurt "$STATE_PATH" "$CURRENTCONTENT"
        return 1
    elif [ "$CURRENTCONTENT" == "$(slurp "$STATE_PATH")" ]; then
        vecho "$TYPE-state for '$FOR': unchanged"
        return 1
    else
        vecho "$TYPE-state for '$FOR': has changed"
        spurt "$STATE_PATH" "$CURRENTCONTENT"
        return 0
    fi
}

# Downloads a URL and outputs to STDOUT
function URLslurp
{
    local URL="$1"
    wget -qq -O- "$URL"
}

# Downloads the headers of a URL and outputs to STDOUT
function URLHeaderSlurp
{
    local URL="$1"
    curl -s -I "$URL"
    ret=$?
    if [ "$?" != "0" ]; then
        _errOut "URLHeaderSlurp: command 'curl -s -I \"$URL\"' failed, returned $ret"
    fi
}

# Outputs the URL state file path to STDOUT
function StateFile
{
    local URI="$1"
    local TYPE="$2"
    local NAME="$(_sanitizeForPath "$URI")"
    if [ "$TYPE" != "" ]; then
        NAME="$(_sanitizeForPath "$TYPE")-$NAME"
    fi
    local STATE_PATH="$PPWT_STATE_DIR/StateFile-$NAME"
    echo "$STATE_PATH"
}

# Reads a file if it exists, otherwise echoes an empty string
function slurp
{
    local FILE="$1"
    if [ -e "$FILE" ]; then
        cat "$FILE"
    else
        echo -n ""
    fi
}

# Write to a file
function spurt
{
    local FILE="$1";shift
    if [ "$FILE" == "" ] || [ ! -d "$(dirname "$FILE")" ]; then
        _errOut "FATAL: spurt: '$FILE' is not a valid path"
    elif [ ! -d "$(dirname "$FILE")" ]; then
        _errOut "FATAL: spurt: the parent directory of the path to '$FILE' does not exist"
    elif [ -e "$FILE" ] && [ ! -w "$(dirname "$FILE")" ]; then
        _errOut "FATAL: spurt: $FILE is not writable"
    fi
    echo -n "$@" > "$FILE"
}

# Run a command, making it silent unless it errors out
function action
{
    local dailyActionsCountFile="$PPWT_STATE_DIR/maxDailyActionsCount"
    local dailyActionsDateFile="$PPWT_STATE_DIR/maxDailyActionsDate"
    local currentActions="$(slurp "$dailyActionsCountFile")"
    local currentDate="$(slurp "$dailyActionsDateFile")"
    local today="$(date '+%Y-%m-%d')"
    if [ "$currentActions" == "" ] || [ "$currentDate" != "$today" ]; then
        currentActions=0
    fi
    let currentActions=$currentActions+1

    spurt "$dailyActionsCountFile" "$currentActions"
    spurt "$dailyActionsDateFile" "$today"

    vecho "Running action: $1"
    local OUT="$( timeout 30s "$@" 2>&1 )"
    local RET="$?"
    date +%s > "$PPWT_STATE_DIR/last-action"
    vecho "It returned $RET"
    if _verbose || [ "$RET" != "0" ]; then
        if [ "$RET" == "124" ]; then
            vecho "The command appears to have timed out, and timeout(1) TERMinated it."
        fi
        vecho "Output:"
        vecho "----------"
        echo "$OUT"
        _logmsgRaw "$OUT"
        vecho "----------"
        if [ "$RET" != "0" ]; then
            return $RET
        fi
    fi
    return 0
}

# Run a command. If it returns nonzero, output some debugging information to
# STDERR and the logfile.
function commandRunner ()
{
    timeout 30s "$@" 2>&1
    local RET="$?"
    if [ "$RET" != "0" ]; then
        if [ "$RET" == "124" ]; then
            echoSTDERR "The command '$@' appears to have timed out, and timeout(1) TERMinated it."
        else
            echoSTDERR "The command '$@' returned nonzero: $RET"
        fi
    fi
    return $RET
}

# Set a schedule to run a script in between
function run_between
{
    if [ "$PPWT_IGNORE_RUNBETWEEN" == "1" ]; then
        return 0
    fi
    local CHECK="$@"
    local EXIT=1
    local FROM
    local TO
    while [ "$1" != "" ]; do
        FROM="$1";shift
        while [ "$FROM" == "or" ] || [ "$FROM" == "and" ]; do
            FROM="$1";shift
        done
        TO="$1";shift
        if _run_between_check "$FROM" "$TO"; then
            EXIT=0
        fi
    done
    if [ "$EXIT" == "1" ]; then
        vecho "Exiting because of run_between $CHECK"
        exit 0
    fi
}

# Select a timeframe to run between
function _run_between_check
{
    if [ "$PPWT_BOOTSTRAP_MODE" == "1" ]; then
        return 0
    fi
    local LOWER="$1"
    local UPPER="$2"
    local NOW_H="$(date +%H)"
    if [ "$LOWER" -gt "$NOW_H" ] || [ "$UPPER" -lt "$NOW_H" ] || [ "$UPPER" == "$NOW_H" ]; then
        return 1
    fi
    return 0
}

# Select which days of the week to run on
function only_weekday ()
{
    if [ "$PPWT_BOOTSTRAP_MODE" == "1" ]; then
        return 0
    fi
    local wday="$(date +%u)"
    for ENTRY in "$@"; do
        if [ "$ENTRY" == "$wday" ]; then
            return 0
        fi
    done
    vecho "Exiting because of only_weekday $@"
    exit 0
}

# Schedule how often to run a script
function schedule
{
    if [ "$PPWT_BOOTSTRAP_MODE" == "1" ]; then
        return 0
    fi
    if [ "$PPWT_IGNORE_SCHEDULE" == "1" ]; then
        return 0
    fi
    local COMPARE
    local MODE="$1"
    local SCHEDULEFILE="$PPWT_STATE_DIR/schedule-$MODE"
    local PREVIOUSTIME="$(slurp "$SCHEDULEFILE")"
    if [ "$PREVIOUSTIME" == "" ]; then
        PREVIOUSTIME="0"
    fi
    local SUBTRACT=0
    local NOW="$(date +%s)"
    if [ "$MODE" == "hourly" ]; then
        SUBTRACT=3000
    elif [ "$MODE" == "daily" ]; then
        SUBTRACT=85800
    elif [ "$MODE" == "always" ]; then
        return
    else
        _errOut "schedule: error: unknown schedule: $1"
    fi
    let COMPARE=$NOW-$SUBTRACT
    if [ $COMPARE -lt $PREVIOUSTIME ];then
        vecho "Exiting because of 'schedule $1': enough time has not yet passed"
        if [ "$PREVIOUSTIME" -gt "0" ]; then
            vecho "Last run: $(date -d @"$PREVIOUSTIME")"
        fi
        exit 0
    fi
    spurt "$SCHEDULEFILE" "$NOW"
}

# Sanitize a string to be safe for use in a filename
function _sanitizeForPath
{
    echo -n "$@" | perl -p -E 's/(\/|:|\s+|\.)/_/g'
}

# Hashes a URL
function _URLHash
{
    local URL="$1"
    URLslurp "$URL"| sha512sum
}

# Echoes a message when in verbose mode
function vecho
{
    if _verbose; then
        _echo "$@"
    fi
    logmsg "$@"
}

# Check if verbose mode is on or not
function _verbose
{
    if [ "$PPWT_VERBOSE" != "" ] && [ "$PPWT_VERBOSE" -gt "0" ]; then
        return 0
    else
        return 1
    fi
}

# Log something
function logmsg
{
    _logmsgRaw "[$(date '+%Y-%m-%d %H:%M:%S') {$$}] $(_echoPrefix)$@"
}

# Echoes a message to STDERR
function echoSTDERR ()
{
    echo "$(_echoPrefix)$@" >&2
    logmsg "$@"
}

# Output a raw string to the logfile
function _logmsgRaw
{
    if [ "$PPWT_LOGFILE" != "" ] && [ -e "$PPWT_LOGFILE" ]; then
        echo "$@" >> "$PPWT_LOGFILE"
    fi
}

# Echo a string with a prefix if possible
function _echo
{
    echo "$(_echoPrefix)$@"
}

# Return a prefix to use for messages
function _echoPrefix
{
    PREFIX=""
    if [ "$PPWT_CURRENT_SCRIPTID" != "" ]; then
        PREFIX="$PPWT_CURRENT_SCRIPTID: "
    fi
    echo -n "$PREFIX"
}

# Error out with a message
function _errOut
{
    _echo "$@"
    exit 1
}

# Verify that all dependencies are present
function verifyDeps ()
{
    local invalid=0
    for dep in curl wget sha512sum basename realpath perl cat date touch mktemp grep egrep; do
        if ! type "$dep" &>/dev/null; then
            invalid=1
            echo "Missing dependency: $dep"
        fi
    done
    if [ "$invalid" != "0" ]; then
        exit 1
    fi
}

# Make sure the parameter provided is a number
# If it is, echoes the parameter, otherwise echoes 0
function mustBeNumber ()
{
    if echo "$1" |egrep -q '^[0-9]+$'; then
        echo "$1"
    else
        echo "0"
    fi
}

# Verify that we're running in bash
if [ "x$BASH_VERSINFO" = "x" ] || [ "$BASH_VERSINFO" -lt "4" ];then
    if [ "x$_PPWT_MAIN" != "x" ]; then
        echo "Shell scripts using shell-lib needs to use #!/bin/bash"
        exit 1
    fi
    echo "Needs to run under bash 4 or later"
    exit 1
fi

# Verify the state directory if we're in a trigger script
# (_PPWT_MAIN is 1 if we're incldued into the runner script)
if [ "$_PPWT_MAIN" != "1" ]; then
    if [ ! -w "$PPWT_STATE_DIR" ]; then
        _echo "FATAL ERROR: $PPWT_STATE_DIR: Is not writable"
        exit 2
    elif [ ! -d "$PPWT_STATE_DIR" ]; then
        _echo "FATAL ERROR: $PPWT_STATE_DIR: Is not a directory"
        exit 2
    fi
fi

if [ "$PPWT_DEBUG" == "-x" ]; then
    set -x
fi

ifndef prefix
# This little trick ensures that make install will succeed both for a local
# user and for root. It will also succeed for distro installs as long as
# prefix is set by the builder.
prefix=$(shell perl -e 'if($$< == 0 or $$> == 0) { print "/usr" } else { print "$$ENV{HOME}/.local"}')

# Some additional magic here, what it does is set BINDIR to ~/bin IF we're not
# root AND ~/bin exists, if either of these checks fail, then it falls back to
# the standard $(prefix)/bin. This is also inside ifndef prefix, so if a
# prefix is supplied (for instance meaning this is a packaging), we won't run
# this at all
BINDIR ?= $(shell perl -e 'if(($$< > 0 && $$> > 0) and -e "$$ENV{HOME}/bin") { print "$$ENV{HOME}/bin";exit; } else { print "$(prefix)/bin"}')
endif

SHELL=/bin/bash
README:
	cat .README.template.md |perl -E 'my $$pod = shift(@ARGV); while(<STDIN>){ s/\{\{\{POD\}\}\}/$$pod/; print };' "$$(pod2markdown manpage.pod|perl -p -E 's/^#/##/')" > README.md
localinstall:
	mkdir -p "$(BINDIR)"
	ln -sf $(shell pwd)/ppwt $(BINDIR)/

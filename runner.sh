#!/bin/bash
# PPWT
# Copyright (C) Eskild Hustvedt 2023
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ "$1" != "--ppwt-internal-exec" ] || [ "$PPWT_SHELL_LIB" == "" ] || [ "$PPWT_VERSION" == ""  ] || [ "$PPWT_CURRENT_SCRIPTID" == "" ] || [ "$PPWT_STATE_DIR" == "" ]; then
    echo "Should not be run directly"
    exit 1
fi
source "$2"
if [ "$_PPWT_APIVERSION_CALLED" == "0" ]; then
    echoSTDERR "WARNING: Script '$2' did not call APIVersion"
elif [ "$_PPWT_APIVERSION_CALLED" != "1" ]; then
    echo "WARNING: Script '$2' appears NOT to have sourced the shell-lib"
fi

# Poor person's web triggers

Want something similar to IFTTT without subscribing to such a service and
without running a big webapp? Can't be bothered to write a script on your own?
Then poor person's web triggers could possibly be what you want. Maybe.

Poor person's web triggers (PPWT) is a set of shell scripts to ease automating
tasks that are triggered by something changing on a web service, for instance
by checking if a calendar or RSS feed has changed and then triggering some
gitlab pages job to rebuild with the updated data.

You could fairly easily do everything PPWT does by scripting from scratch
yourself, but PPWT gives you a starting point and some tools to help make the
whole thing more reliable and simple to write. In particular, for shell scripts
the included library eases writing the scripts a lot by factoring out much of
the complexity involved in detecting changes and tracking them over time.

To schedule a task, just drop a shell script in triggers.d and use the shell
script API below to perform actions. If you don't want to use shell scripts,
you can drop anything in triggers.d, but you'll have to write more boilerplate
yourself. Right now it checks for *.sh, *.pl and *.raku.

You can use a git repository as your triggers.d by simply removing the
pre-created directory and cloning a repo there instead. Add an empty '.create'
to make sure git doesn't think you have made a change to the main PPWT repo.

## Design philosophy

PPMT is written to have a **low** impact on a running system. Scripts are
limited to 1 minute of execution time (with an additional minute if it's
ignoring SIGTERM, before it will be killed). They run with a niceness of 19 and
with priority 7 (the lowest) of IO class "best-effort" (the default on Linux).

It's intended to have **few dependencies** and be mostly clone and run.

It's not built to *scale* above hobbyist levels. It's meant to make hacks
simpler to write and easy to maintain. It's good enough™ for personal use. It's
also meant to be fire and forget. All it needs is a working cron daemon and
you're off.

## Configuration, command-line options etc.

## NAME

PPWT - Poor person's web triggers

## SYNOPSIS

**ppwt** \[_OPTIONS_\]

## DESCRIPTION

Poor person's web triggers (PPWT) is a set of shell scripts to ease automating
tasks that are triggered by something changing on a web service, for instance
by checking if a calendar or RSS feed has changed and then triggering some
gitlab pages job to rebuild with the updated data.

To schedule a task, just drop a shell script in triggers.d and use the shell
script API below to perform actions. If you don't want to use shell scripts,
you can drop anything in triggers.d, but you'll have to write more boilerplate
yourself. Right now it checks for \*.sh, \*.pl and \*.raku.

See the _SHELL SCRIPT API_ section for information about how to write your
scripts.

## OPTIONS

- **-h, --help**

    Display the help screen and exit.

- **--version**

    Output the version number and exit.

- **-v, --verbose**

    Be verbose.

- **--log** _FILE_

    Enable logging to FILE.

    May not me combined with _--log-actions_ or _--log-errors_.

- **--log-actions** _FILE_

    Enable logging to FILE, but only write to the log if an _action_ was actually
    performed, or an error occurred. This avoids _ppwt_ writing to the logfile to
    inform you that nothing was done.

    May not me combined with _--log_ or _--log-errors_.

- **--log-errors** _FILE_

    Enable logging to FILE, but only write to the log if an an error occurred.

    May not me combined with _--log_ or _--log-actions_.

- **--ignore-schedule**

    Ignores calls to 'schedule' in scripts, effectively forcing all schedules to
    match. This can be used to force scripts to run. See also
    \`--ignore-runbetween\`.

- **--ignore-runbetween**

    Ignores calls to 'run\_between' and 'only\_weekday' in scripts, effectively
    forcing all run\_between and only\_weekday to match. This can be used to force
    scripts to run. See also \`--ignore-schedule\`.

- **--run-script** _FILE_

    Run the script at FILE instead of all files. You can combine this with \`-x\`
    and/or \`--ignore-schedule\` and \`--ignore-runbetween\` to test a single script.

- **-x, --trace**

    Enable _set -x_ tracing in both PPWT and trgger scripts.

## CONFIGURATION

Configure the _ppwt_ script to run through cron. You can chose your own
intervals, but hourly should be sufficient for most things. The lowest interval
is every 15 minutes, dropping below that may make schedules run more often than
requested (since there's a builtin slack of 10 minutes to account for scripts
being run in order and sometimes taking longer). Drop your scripts in
triggers.d with the execute bit set and you're off.

## SHELL SCRIPT API

To use the shell script API you need to add the following boilerplate to the
top of your shell script:

    . "$PPWT_SHELL_LIB" || exit 1
    APIVersion 2

This will load the shell library and verify that it supports an API of the
version requested. APIVersion is currently 2. Then you can use the functions
listed below.

Also note that your script should use #!/bin/bash not #!/bin/sh

### schedule PARAM

This function sets the schedule for how often you want the script to run. The
_schedule_ function will _exit_ if not enough time has passed.

Valid parameters are: "hourly", "daily", "always"

Note that for hourly and daily, it's not exactly 1 and 24 hours. It could be slightly
less to account for some scripts taking longer at certain intervals.

You can also combine multiple schedule's. For instance have a schedule "hourly"
at the top of a script, followed by a schedule "daily" at the bottom. PPWT will
keep track of both (but the second schedule will only be checked if the first
schedule matches). Under the hood each "schedule" works by exiting your script
if the schedule doesn't match.

Example: _schedule "hourly"_

### run\_between MIN\_HOUR MAX\_HOUR

This function sets a timeframe in between this job can run. The first parameter
is the earliest hour to run, and the second parameter is the hour of which not
to run after. _run\_between "7" "20"_ will execute the script if the current
time is between 7:00-19:59. If the current time is outside of the requested
timeframe then it will exit.

You can provide multiple timeframes: _run\_between 7 18 or 3 4_ will run
between 7:00 and 17:59 and 03:00 and 03:59. The keyword "or" is syntactic sugar
and may be omitted (or replaced with "and").

You can combine run\_between and schedule, for instance to run a job hourly but
only in a certain timeframe.

Example: _schedule 7 18_ - runs the job if the time is between 7:00-17:59.

### only\_weekday DAY1 DAY2

This function sets which days of the week this job can run. It takes up to
seven numbers as its parameters. The numbers correspond to the day of the week,
starting with monday as 1, and ending with sunday as 7.

You can combine only\_weekday and run\_between.

Example: _only\_weekday 1 2 3 4_ - runs the job on monday, tuesday, wednesday
and thursday.

### max\_daily\_actions MAX

If MAX or more _action_s have been run in this script today, then skip the script
for the rest of the day. The number of actions resets at midnight.

### spurt FILE CONTENT

Writes CONTENT to FILE.

Example: _spurt "test" "hello world"_ writes "hello world" to "./test".

### slurp FILE

Reads from FILE, if it exist, otherwise functions as a no-op (so it's safe to
slurp a file that doesn't exist, you'll just get an empty string "" returned).

### StateFile FOR TYPE

Echo the path to a file where to store state for the URI "_FOR_" of the type
"_TYPE_". TYPE can be an arbitrary string. FOR is usually a URL. This is also
used internally to store state data.

Example: _StateFile https://example.com TestingState_

### URLslurp URL

Download URL and echo it to STDOUT

### hasChangedURL URL1 URL2

Returns 0 if any of the provided URLs has changed since last time. Returns
1 if nothing has changed.

### hasChangedCalendarURL URL

Variant of hasChangedURL that applies some workarounds to detect changes in
iCalendar files. Returns 0 if the URLs provided has changed since last time.
Returns 1 if nothing has changed.

### hasChangedURLHeaders URL1 URL2

Variant of hasChangedURL that checks the ETAG and Last-Modified headers for
changes instead of downloading the entire URL. You should make sure the
upstream server sends at least one of those two if you want to use
hasChangedURLHeaders. Given that the server sends at least one of those, this
works identically to hasChangedURL. Whenever possible, you should prefer
hasChangedURLHeaders.

### mustBeNumber VAR

This checks if VAR is a number. If it is, then VAR is echoed. If it isn't, then
0 is echoed. Can be used to make numeric checks safe when the variable may for
some reason contain invalid data.

## THE STATE DIRECTORY

When using the shell script integration PPWT will write files to a state/
subdirectory named after the script. In this subdirectory any permanent state
(such as that needed for _hasChanged\*_ and _schedule_ will be written. You may
empty this directory at will, but note that this will invalidate any
_schedule_s that you have in your scripts, and _hasChanged\*_ will reset to
whatever the current content is (but _hasChanged\*_ will not re-trigger). You
shouldn't need to empty this directory, however, as all of the content in it
takes up an incredibly small amount of disk space (for instance, _hasChanged\*_
stores checksums, and not actual content).

## AUTHOR

**PPWT** is written by Eskild Hustvedt _&lt;code @ zerodogg dot org_>

## LICENSE AND COPYRIGHT

Copyright (C) Eskild Hustvedt 2020

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
